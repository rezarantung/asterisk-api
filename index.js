var express = require("express");
var AGIServer = require('agi-node').AGIServer;
var apn = require('apn');
var FCM = require('fcm-node');
var app = express();
const bodyParser = require('body-parser')
const multer = require('multer') // v1.0.5
const upload = multer() // for parsing multipart/form-data
var mysql = require('mysql')
var pool = mysql.createPool({
  connectionLimit: 10,
  host: '45.76.185.238',
  user: 'asterisk',
  password: '45t31215K#',
  database: 'asterisk',
  charset: 'utf8mb4'
})

app.use(bodyParser.json()) // for parsing application/json
app.use(bodyParser.urlencoded({ extended: true })) // for parsing application/x-www-form-urlencoded

//show all peers
app.get("/peers", (req, res, next) => {
    pool.getConnection(function (err, connection) {
        let result = {
            'status' : '',
            'peers' : []
        }
        
        if (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        }

        connection.query('SELECT * FROM sip_peers', function (err, rows, fields) {
            if(err) {
                result.status = 'error'
            } else {
                result.status = 'success'
                if(rows.length > 0) {
                    let peers = []
                    for(var i = 0; i < rows.length; i++) { 
                        peers.push({
                            name: rows[i].name,
                            password: rows[i].secret
                        })
                    }
                    result.peers = peers
                }
            }
            res.json(result)
            connection.release();
        })

        connection.on('error', function (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        });
    });
});

//search peer by username
app.get("/peer/:user", (req, res, next) => {
    pool.getConnection(function (err, connection) {
        let result = {
            'status' : 'error',
            'data' : {
                'avatar' : "",
                'name' : "",
                'phone' : "",
                'email' : ""
            }
        }
        
        if (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        }

        connection.query(`SELECT * FROM sip_peers WHERE username LIKE '%${req.params.user}%'`, function (err, rows, fields) {
            if(err) {
                result.status = 'error'
            } else {
                if(rows.length > 0) {
                    result.status = 'success'
                    result.data.avatar = rows[0].avatar;
                    result.data.name = rows[0].display_name;
                    result.data.phone = rows[0].username;
                    result.data.email = rows[0].email;
                }
            }
            res.json(result)
            connection.release();
        })

        connection.on('error', function (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        });
    });
});

//check is valid peer
app.get("/isvalid/:user", (req, res, next) => {
    pool.getConnection(function (err, connection) {
        let result = {
            'status' : 'error',
            'isValid' : false
        }
        
        if (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        }

        connection.query(`SELECT * FROM sip_peers WHERE username LIKE '%${req.params.user}%' AND (email IS NOT NULL AND email != 'undefined') AND (avatar IS NOT NULL AND avatar != 'undefined') AND (display_name IS NOT NULL AND display_name != 'undefined')`, function (err, rows, fields) {
            if(err) {
                result.status = 'error'
            } else {
                if(rows.length > 0) {
                    result.status = 'success'
                    result.isValid = true;
                }
            }
            res.json(result)
            connection.release();
        })

        connection.on('error', function (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        });
    });
});

//add peer
app.post("/add", upload.array(), (req, res, next) => {
    pool.getConnection(function (err, connection) {
        let result = {
            'status' : 'success'
        }
        
        if (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        }

        let sql1 = `INSERT INTO sip_peers (name, username, secret, context, host, nat, qualify, type, email, avatar, display_name, callerid) 
                VALUES ('${req.body.user}', '${req.body.user}', '${req.body.secret}', 'contact', 'dynamic', 'yes', 'no', 'friend', '${req.body.email}', '${req.body.avatar}', '${req.body.name}', '"${req.body.name}" \<${req.body.user}\>');`;
        let sql2 = `INSERT INTO dialplan_exten (context, exten, priority, app, appdata)
                    VALUES ('contact', '${req.body.user}', '1', 'Dial', 'SIP/${req.body.user},45');`

        connection.query(`SELECT * FROM sip_peers WHERE username LIKE '%${req.body.user}%'`, function (err, rows, fields) {
            if(err) {
                result.status = 'error'
            } else {
                if(rows.length > 0) {
                    result.status = 'user already exist'
                } else {
                    connection.beginTransaction(function(err) {
                        if (err) { 
                            result.status = 'error'
                            throw err; 
                        }
                        
                        connection.query(sql1, function (err, rows, fields) {
                            if(err) {
                                connection.rollback(function() {
                                    result.status = 'error'
                                    throw err;
                                });
                            } else {
                                connection.query(sql2, function (err, rows, fields) {
                                    if(err) {
                                        connection.rollback(function() {
                                            result.status = 'error'
                                            throw err;
                                        });
                                    }
                                })
                            }

                            connection.commit(function(err) {
                                if (err) { 
                                    connection.rollback(function() {
                                        result.status = 'error'
                                        throw err;
                                    });
                                }
                                connection.end();
                            });
                        })
                    });
                }
            }
            res.json(result)
            connection.release();
        })

        connection.on('error', function (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        });
    });
});

//update peer
app.post("/update", upload.array(), (req, res, next) => {
    pool.getConnection(function (err, connection) {
        let result = {
            'status' : 'error'
        }
        
        if (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        }

        let sql = `UPDATE sip_peers SET email = '${req.body.email}', avatar = '${req.body.avatar}', display_name = '${req.body.name}' WHERE username = '${req.body.user}';`;

        connection.query(sql, function (err, rows, fields) {
            if(err) {
                result.status = 'error'
            } else {
                result.status = 'success'
            }
            res.json(result)
            connection.release();
        })

        connection.on('error', function (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        });
    });
});

//update peer token
app.post("/update_token", upload.array(), (req, res, next) => {
    pool.getConnection(function (err, connection) {
        let result = {
            'status' : 'error'
        }
        
        if (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        }

        let sql = "UPDATE dialplan_exten SET appdata = 'user="+req.body.token+","+req.body.user+",${CALLERID(num)},${SIPCALLID},${CDR(uniqueid)}' WHERE exten = '"+req.body.user+"' AND app = 'Set';";

        connection.query(sql, function (err, rows, fields) {
            if(err) {
                result.status = 'error'
            } else {
                result.status = 'success'
            }
            res.json(result)
            connection.release();
        })

        connection.on('error', function (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        });
    });
});

//delete peer
app.delete("/delete", upload.array(), (req, res, next) => {
    pool.getConnection(function (err, connection) {
        let result = {
            'status' : 'success'
        }
        
        if (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        }

        let sql1 = `DELETE FROM sip_peers WHERE username = '${req.body.user}';`;
        let sql2 = `DELETE FROM dialplan_exten WHERE exten = '${req.body.user}';`

        connection.query(sql1, function (err, rows, fields) {
            if(err) {
                result.status = 'error'
            } else {
                connection.query(sql2, function (err, rows, fields) {
                    if(err) {
                        result.status = 'error'
                    }
                })
            }
            res.json(result)
            connection.release();
        })

        connection.on('error', function (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        });
    });
});

//add agi peer
app.post("/add_agi", upload.array(), (req, res, next) => {
    pool.getConnection(function (err, connection) {
        let result = {
            'status' : 'success'
        }
        
        if (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        }

        var callerId = "${CALLERID(num)}";
        var sipCallId = "${SIPCALLID}";
        var uniqueId = "${CDR(uniqueid)}";

        let sql1 = `INSERT INTO sip_peers (name, username, secret, context, host, nat, qualify, type, email, avatar, display_name, callerid) 
                VALUES ('${req.body.user}', '${req.body.user}', '${req.body.secret}', 'contact', 'dynamic', 'yes', 'no', 'friend', '${req.body.email}', '${req.body.avatar}', '${req.body.name}', '"${req.body.name}" \<${req.body.user}\>');`;
        let sql2 = `INSERT INTO dialplan_exten (context, exten, priority, app, appdata) VALUES ('contact', '${req.body.user}', '1', 'Set', 'user=${req.body.token},${req.body.user},${callerId},${sipCallId},${uniqueId}');`
        let sql3 = `INSERT INTO dialplan_exten (context, exten, priority, app, appdata) VALUES ('contact', '${req.body.user}', '2', 'AGI', 'agi://localhost:3001');`

        connection.query(`SELECT * FROM sip_peers WHERE username LIKE '%${req.body.user}%'`, function (err, rows, fields) {
            if(err) {
                result.status = 'error'
            } else {
                if(rows.length > 0) {
                    result.status = 'user already exist'
                } else {
                    connection.beginTransaction(function(err) {
                        if (err) { 
                            result.status = 'error'
                            throw err; 
                        }
                        connection.query(sql1, function (err, rows, fields) {
                            if(err) {
                                connection.rollback(function() {
                                    result.status = 'error'
                                    throw err;
                                });
                            } else {
                                connection.query(sql2, function (err, rows, fields) {
                                    if(err) {
                                        connection.rollback(function() {
                                            result.status = 'error'
                                            throw err;
                                        });
                                    } else {
                                        connection.query(sql3, function (err, rows, fields) {
                                            if(err) {
                                                connection.rollback(function() {
                                                    result.status = 'error'
                                                    throw err;
                                                });
                                            } 
                                        })
                                    }
                                })
                            }
            
                            connection.commit(function(err) {
                                if (err) { 
                                    connection.rollback(function() {
                                        result.status = 'error'
                                        throw err;
                                    });
                                }
                                connection.end();
                            });
                        })
                    });
                }
            }
            res.json(result)
            connection.release();
        })

        connection.on('error', function (err) {
            result.status = 'error'
            res.json(result)
            connection.release();
        });
    });
});

//send notif apn
app.post("/send_apn", upload.array(), (req, res, next) => {
    var token = req.body.token;
    var options = new apn.Provider({
        cert: 'cert/chataja-voip.pem',
        key: 'cert/chataja-voip.key.pem'
    });

    var note = new apn.Notification();

    note.expiry = Math.floor(Date.now() / 1000) + 60;
    note.badge = 3;
    note.sound = "ping.aiff";
    note.alert = "You have a new message";
    note.payload = {
        "aps":{
            "alert":"Hello!", 
            "badge":1, 
            "sound":"default"
        }
    };
    note.priority = 10;  
    note.pushType = "voip"; //or alert?
    note.topic = "chat.kiwari.mobile.voip";

    options.send(note, token).then( (err,result) => {
        if(err) return res.json(JSON.stringify(err));
        return res.json(JSON.stringify(result))
    });
});

//////////////////////////////////////////////////////////////////////////arief
//update_avatar
app.patch("/update_avatar", upload.array(), function (req, res, next) {
    pool.getConnection(function (err, connection) {
        let result = {
            "status" : "error"
        }

        let sql = "UPDATE sip_peers SET avatar = '${req.body.avatar}' WHERE username = '${req.body.user}'";

        if (req.body.avatar==null || req.body.avatar == "" || req.body.user == null || req.body.user == ""){
            res.json(result);
        } else{
            connection.query(sql, function (err, data) {
                if (!err){
                    result.status = "success";
                }
                res.json(result);
                connection.release();
            })
        }
    })
})

app.post("/get_or_add", upload.array(), (req, res, next) => {
    pool.getConnection(function (err, connection) {
        let result = {
            "status" : "error"
        }
        let sql1 = "SELECT * FROM sip_peers WHERE username LIKE '${req.body.user}'";
        let sql2 = `INSERT INTO sip_peers (name, username, secret, context, host, nat, qualify, type, email, avatar, display_name, callerid) 
                            VALUES ('${req.body.user}', '${req.body.user}', '${req.body.secret}', 'contact', 'dynamic', 'yes', 'no', 'friend', '${req.body.email}', '${req.body.avatar}', '${req.body.name}', '"${req.body.name}" \&lt;${req.body.user}\&gt;')`;

        let sql3 = `UPDATE sip_peers SET email = '${req.body.email}', avatar = '${req.body.avatar}', display_name = '${req.body.name}' WHERE username = '${req.body.user}'`;
        if (req.body.user != null){
            connection.query(sql1, function (err, data) {
                if (!err){
                    if (data==""){
                        console.log("ini kosong, berarti masukin data nya ke sini")
                        connection.query(sql2, function(err, data){
                            if (!err){
                                console.log("ini success")
                                result.status = "success";

                            }
                            res.json(result)

                        })
                    }
                    // console.log(data)
                    if (data!=""){
                        console.log("halo, data sudah ada")
                        connection.query(sql3, function(err, data){
                            if (!err){
                                console.log("ini update success")
                                result.status = "success"
                            }
                            res.json(result)
                        })

                    }
                }
            })
            connection.release();
        }
    })
})
///////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////AGI

async function agiScript(channel) {
    var user = channel.getVariable('user').split(',');

    var sip_header_call_id = channel.channelStatus();
    var sip_header = channel.getVariable('SIP_HEADER(Session-ID)');
    var registry =  channel.getVariable('DB(SIP/Registry/' + user[1] + ')');

    console.log('CHANNEL STATUS', channel.channelStatus());
    console.log('SIP_HEADER ' + sip_header + ' ' + channel.getVariable('UNIQUEID') + ' ' + channel.getVariable('SIPCALLID'));
    console.log('Registry ' + registry);

    //////////////////////////////////////////////////////////get caller data from db
    pool.getConnection(function (err, connection) {
        if (err) {
            console.log(err);
            connection.release();
        }

        connection.query(`SELECT * FROM sip_peers WHERE username LIKE '${user[2]}'`, function (err, rows, fields) {
            if(err) {
                console.log(err);
            } else {
                /////////////////////////////////////////////////////send apn notif
                var token = user[0];
                var options = new apn.Provider({
                    cert: 'cert/chataja-voip.pem',
                    key: 'cert/chataja-voip.key.pem',
                    production: true
                });

                var note = new apn.Notification();

                note.expiry = 0;

                if(rows.length > 0) {
                    for(var i = 0; i < rows.length; i++) { 
                        note.payload = {
                            "caller" : {
                                // "sip_header_call_id" : sip_header_call_id,
                                "sip_call_id" : user[3],
                                // "cdr_unique_id" : user[4],
                                "caller_id" : rows[i].callerid,
                                "name" : rows[i].display_name, 
                                "num" : rows[i].name, 
                                "avatar" : rows[i].avatar
                            }
                        };
                    }
                }

                note.priority = 10;  
                note.pushType = "voip"; //or alert?
                note.topic = "chat.kiwari.mobile.voip";
                
            }
            
            options.send(note, token).then( (err,result) => {
                if (err) {
                    console.log(JSON.stringify(err));
                } else {
                    console.log(JSON.stringify(result)); 
                }
                
                options.shutdown();
            });
            
            console.log('result : ' + rows.length);
            connection.release();
        })

        connection.on('error', function (err) {
            console.log(err);
            connection.release();
        });
    });
    channel.exec("WAIT", 3);
    channel.exec("DIAL", "SIP/"+user[1]+",45");
}

var server = new AGIServer(agiScript, 3001);
app.listen(3000, () => {
    console.log("Server running on port 3000");
});
